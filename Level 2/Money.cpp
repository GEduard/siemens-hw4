#include <iostream>
#include "Money.h"

Money::Money()
{
	euros = 0;
	centimes = 0;
}

Money::Money(int Euros, int Centimes)
{
	if (Centimes >= 100)
		throw "Invalid parameter value!";
	euros = Euros;
	centimes = Centimes;
}

int Money::getEuros()
{
	return euros;
}

int Money::getCents()
{
	return centimes;
}

void Money::setEuros(int Euros)
{
	euros = Euros;
}

void Money::setCents(int Centimes)
{
	centimes = Centimes;
}

void Money::print()
{
	std::cout << this->getEuros() << "," << this->getCents() << " Euros" << std::endl;
}

Money Money::operator +(Money &m)
{
	Money money;
	if (this->getCents() + m.getCents() < 100)
	{
		money.setEuros(this->getEuros() + m.getEuros());
		money.setCents(this->getCents() + m.getCents());
	}
	else
	{
		int nEuros = (this->getCents() + m.getCents())/100;
		money.setEuros(this->getEuros() + m.getEuros() + nEuros);
		money.setCents((this->getCents() + m.getCents()) % 100);
	}
	return money;
}

Money Money::operator -(Money& m)
{
	Money money;
	if (this->euros < m.euros)
		throw "Can't substract!";
	else
	{
		float firstVal, secondVal, newEVal, newCVal;
		firstVal = (float)this->getEuros() + ((float)this->getCents() / 100);
		secondVal = (float)m.getEuros() + ((float)m.getCents() / 100);
		firstVal -= secondVal;
		newCVal = modf(firstVal, &newEVal);
		money.setEuros(int(newEVal));
		money.setCents((int)(newCVal * 100));
	}
	return money;
}

Money Money::operator /(Money& m)
{
	Money money;
	float firstVal, secondVal, newEVal, newCVal;
	firstVal = (float)this->getEuros() + ((float)this->getCents() / 100);
	secondVal = (float)m.getEuros() + ((float)m.getCents() / 100);
	firstVal /= secondVal;
	newCVal = modf(firstVal, &newEVal);
	money.setEuros(int(newEVal));
	money.setCents((int)(newCVal * 100));
	return money;
}

Money Money::operator *(Money& m)
{
	Money money;
	float firstVal, secondVal, newEVal, newCVal;
	firstVal = (float)this->getEuros() + ((float)this->getCents() / 100);
	secondVal = (float)m.getEuros() + ((float)m.getCents() / 100);
	firstVal *= secondVal;
	newCVal = modf(firstVal, &newEVal);
	money.setEuros(int(newEVal));
	money.setCents((int)(newCVal * 100));
	return money;
}

bool Money::operator !=(Money &m)
{
	if (this->getEuros() == m.getEuros() && this->getCents()==m.getCents())
		return false;
	return true;
}

bool Money::operator ==(Money &m)
{
	if (this->getEuros() != m.getEuros() && this->getCents() != m.getCents())
		return false;
	return true;
}
