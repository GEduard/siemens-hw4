// CreditCard.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "Money.h"
#include "CreditCard.h"

CreditCard::CreditCard(std::string cOwner, std::string cNumber, std::map<const std::string, Money*> cTransactions)
{
	cardOwner = cOwner;
	cardNumber = cNumber;
	cardTransactions = cTransactions;
}

CreditCard::~CreditCard() 
{
	cardTransactions.clear();
}

void CreditCard::print()
{
	if (cardTransactions.size() == 0)
		std::cout << "Nu exista tranzactii!" << std::endl << std::endl;
	else
	{
		for (const auto& Trans : cardTransactions)
		{
			std::cout << Trans.first << " "; 
			Trans.second->print();
		}
		std::cout << std::endl;
	}
}

void CreditCard::charge(const std::string item, Money* money)
{
	cardTransactions.insert(std::pair<const std::string, Money*>(item, money));
}

void CreditCard::charge(const std::string item, int euros, int cents)
{
	Money* money = new Money(euros, cents);
	cardTransactions.insert(std::pair<const std::string, Money*>(item, money));
}

int main()
{
	std::map<const std::string, Money*> cMap;
	CreditCard card("Gothard Eduard", "1234 5678 9876 5432", cMap);
	card.print();
	card.charge("Laptop", new Money(650, 45));
	card.charge("Tricou", 5, 20);
	card.print();
    return 0;
}
