#pragma once

#include <map>
#include <string>

class CreditCard
{
	private:
		std::string cardOwner, cardNumber;

		std::map<const std::string, Money*> cardTransactions;

	public:
		CreditCard(std::string cOwner, std::string cNumber, std::map<const std::string, Money*>);

		~CreditCard();

		void print();

		void charge(const std::string item, Money* money);

		void charge(const std::string item, int euros, int cents);
};