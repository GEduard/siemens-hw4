class Money
{
	private:
		int euros, centimes;

	public:
		Money();

		Money(int Euros, int Centimes);

		int getEuros();

		int getCents();

		void setEuros(int Euros);

		void setCents(int Centimes);

		void print();

		Money operator +(Money& m);

		Money operator -(Money &m);

		Money operator /(Money &m);

		Money operator *(Money &m);

		bool operator !=(Money &m);

		bool operator ==(Money &m);
};