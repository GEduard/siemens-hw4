#include "Customer.h"

Customer::Customer() {}

Customer::Customer(std::string Name)
{
	name = Name;
}

std::string Customer::getName()
{
	return name;
}

bool Customer::isMember()
{
	return member;
}

void Customer::setMember(bool Member)
{
	member = Member;
}

std::string Customer::getMemberType()
{
	return memberType;
}

void Customer::setMemberType(std::string type)
{
	memberType = type;
}

std::string Customer::toString()
{
	std::string customerDetails;
	if (!isMember())
	{
		customerDetails.append("Customer ");
		customerDetails.append(getName());
		customerDetails.append(" not a member!");
	}
	else
	{
		customerDetails.append(getName());
		customerDetails.append(": ");
		customerDetails.append(memberType);
		customerDetails.append(" member!");
	}
	return customerDetails;
}