#pragma once

#include <string>

class Customer
{
	private:
		std::string name, memberType;

		bool member = false;

	public:
		Customer();

		Customer(std::string Name);
		
		std::string getName();

		bool isMember();

		void setMember(bool Member);

		std::string getMemberType();

		void setMemberType(std::string type);

		std::string toString();
};