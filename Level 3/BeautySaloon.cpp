// BeautySaloon.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "Visit.h"
#include "DiscountRate.h"

int main()
{
	boost::gregorian::date visitDate(2016, boost::gregorian::Apr, 10);
	Customer customer("Gothard Eduard");
	DiscountRate discountRate;
	std::cout << customer.toString() << std::endl;
	if (!customer.isMember())
	{
		customer.setMember(true);
		customer.setMemberType("Gold");
	}
	std::cout << customer.toString() << std::endl;
	Visit visit(customer.getName(), visitDate);
	visit.setProductExpense(85.57);
	visit.setServiceExpense(37.52);
	std::cout << visit.toString() << std::endl;
	std::cout << "Calculating discounts!"<<std::endl;
	std::cout << "Product discount rate:"<< discountRate.getProductDiscountRate(customer.getMemberType()) << std::endl;
	std::cout << "Service discount rate:" << discountRate.getServiceDiscountRate(customer.getMemberType()) << std::endl;
	visit.setProductExpense(visit.getProductExpense() - (visit.getProductExpense() * discountRate.getProductDiscountRate(customer.getMemberType())));
	visit.setServiceExpense(visit.getServiceExpense() - (visit.getServiceExpense() * discountRate.getServiceDiscountRate(customer.getMemberType())));
	std::cout << "Discounts applied! New prices:"<<std::endl;
	std::cout << visit.toString() << std::endl;
    return 0;
}
