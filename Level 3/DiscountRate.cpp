#include "DiscountRate.h"

DiscountRate::DiscountRate()
{

}

double DiscountRate::getProductDiscountRate(std::string type)
{
	if (type == "Premium")
		return serviceDiscountPremium;
	else if (type == "Gold")
		return serviceDiscountGold;
	else if (type == "Silver")
		return serviceDiscountSilver;
	else throw "Invalid type!";
}

double DiscountRate::getServiceDiscountRate(std::string type)
{
	if (type == "Premium")
		return productDiscountPremium;
	else if (type == "Gold")
		return productDiscountGold;
	else if (type == "Silver")
		return productDiscountSilver;
	else throw "Invalid type!";
}