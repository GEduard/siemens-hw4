#include "Customer.h"
#include <boost/date_time/gregorian/gregorian.hpp>

class Visit: public Customer
{
	private:
		boost::gregorian::date date;

		Customer customer;

		double serviceExpense, productExpense;

	public:
		Visit(std::string name, boost::gregorian::date visitDate);

		std::string getName();

		double getServiceExpense();

		void setServiceExpense(double ex);

		double getProductExpense();

		void setProductExpense(double ex);

		double getTotalExpense();

		std::string toString();
};