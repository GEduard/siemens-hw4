#include "Visit.h"
#include <iomanip>

Visit::Visit(std::string name, boost::gregorian::date visitDate)
{
	customer = Customer(name);
	date = visitDate;
}

std::string Visit::getName()
{
	std::string visitor;
	visitor.append("Visitor: ");
	visitor.append(customer.getName());
	return visitor;
}

double Visit::getServiceExpense()
{
	return serviceExpense;
}

void Visit::setServiceExpense(double ex)
{
	serviceExpense = ex;
}

double Visit::getProductExpense()
{
	return productExpense;
}

void Visit::setProductExpense(double ex)
{
	productExpense = ex;
}

double Visit::getTotalExpense()
{
	return getProductExpense() + getServiceExpense();
}

std::string Visit::toString()
{
	std::string visitDetails;
	std::stringstream prodExpense, servExpense, totalExpense;
	prodExpense << std::fixed << std::setprecision(2) << getProductExpense();
	servExpense << std::fixed << std::setprecision(2) << getServiceExpense();
	totalExpense << std::fixed << std::setprecision(2) << getTotalExpense();
	visitDetails.append(getName());
	visitDetails.append(" - ");
	visitDetails.append("Date: ");
	visitDetails.append(to_simple_string(date));
	visitDetails.append(" Total Expense: ");
	visitDetails.append(totalExpense.str());
	visitDetails.append(" Product Expense: ");
	visitDetails.append(prodExpense.str());
	visitDetails.append(" Service Expense: ");
	visitDetails.append(servExpense.str());
	return visitDetails;
}