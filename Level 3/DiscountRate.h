#include <string>

class DiscountRate
{
	private:
		const double serviceDiscountPremium = 0.2;

		const double serviceDiscountGold = 0.15;

		const double serviceDiscountSilver = 0.1;

		double productDiscountPremium = 0.1;

		double productDiscountGold = 0.1;

		double productDiscountSilver = 0.1;

	public:
		DiscountRate();

		double getServiceDiscountRate(std::string type);

		double getProductDiscountRate(std::string type);
};